import { useState } from "react";

const SimpleInput = (props) => {
  const [enteredInput, setEnteredInput] = useState("0,00")
  
  const getComputedValue = (currentValue, keyValue) => {
    const keys = ["0","1","2","3","4","5","6","7","8","9","Backspace"]
    if(!keys.includes(keyValue)){
      return currentValue
    }

    const mergedValue = keyValue === "Backspace" ? currentValue.slice(0, -1) : currentValue + keyValue
    const data = mergedValue.replace(",", "")
    const first = data.substring(0, (data.length - 2))
    if(isNaN(Math.abs(first))) {
      return currentValue
    }
    const second = data.substring(data.length - 2)
    return(Math.abs(first) + "," + second)
  }

  const onKeyDownHandler = (event) => {
    event.preventDefault()
    setEnteredInput((preventValue) => {
      return(getComputedValue(preventValue, event.key))
    })
  }

  const onChangeHandler = (event) => {
    return false
  }
  return (
    <form>
      <div className='form-control'>
        <label htmlFor='price'>Your Price</label>
        <input type='text' id='price' 
        value={enteredInput}
        onKeyDown={onKeyDownHandler}
        onChange={onChangeHandler}
        />
      </div>
    </form>
  );
};

export default SimpleInput;
